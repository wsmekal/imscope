
// standard libraries
#include <imgui.h>
#include <implot.h>
#include <string>
#include <map>

// imGui and SDL libraries
#include "imgui_impl_sdl.h"
#include "imgui_impl_opengl3.h"
//#include "Fonts/Fonts.h"
#include <SDL.h>
#if defined(IMGUI_IMPL_OPENGL_ES2)
#include <SDL_opengles2.h>
#else
#include <SDL_opengl.h>
#endif


// Barebones Application Framework
class imApp 
{
public:
  // Constructors
  imApp();
  imApp(std::string title, int w, int h);
    
  // Destructor.
  virtual ~imApp();

  void imInit();
    
  // Called at top of run
  virtual void atStart()
    { }
  // Called at top of run
  virtual void atExit()
    { }

  // Update, called once per frame.
  virtual void Update() = 0;
    
  // Runs the app.
  void Run();
    
  // Get window size
  ImVec2 GetWindowSize();

  bool done;

private:
  void StyeColorsApp();

  SDL_Window* window;
  SDL_GLContext gl_context;

  int mWidth;
  int mHeight;

  std::string mTitle;
  ImVec4 ClearColor;                    // background clear color
  //std::map<std::string, ImFont*> Fonts;  // font map
};