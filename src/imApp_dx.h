
// standard libraries
#include <imgui.h>
#include <implot.h>
#include <string>
#include <map>

// imGui and directx libraries
#include "imgui_impl_win32.h"
#include "imgui_impl_dx12.h"
#include <d3d12.h>


// Barebones Application Framework
class imApp 
{
public:
  // Constructors
  imApp();
  imApp(std::string title, int w, int h);
    
  // Destructor.
  virtual ~imApp();

  void imInit();
    
  // Called at top of run
  virtual void atStart()
    { }
  // Called at top of run
  virtual void atExit()
    { }

  // Update, called once per frame.
  virtual void Update() = 0;
    
  // Runs the app.
  void Run();
    
  // Get window size
  ImVec2 GetWindowSize();

  bool done;

private:
  void StyeColorsApp();

  HWND hwnd;
  WNDCLASSEX wc;

  int mWidth;
  int mHeight;

  std::string mTitle;
  ImVec4 ClearColor;                    // background clear color
  //std::map<std::string, ImFont*> Fonts;  // font map
};