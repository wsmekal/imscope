// Dear ImGui: standalone example application for SDL2 + OpenGL
// (SDL is a cross-platform general purpose library for handling windows, inputs, OpenGL/Vulkan/Metal graphics context creation, etc.)
// If you are new to Dear ImGui, read documentation from the docs/ folder + read the top of imgui.cpp.
// Read online: https://github.com/ocornut/imgui/tree/master/docs

// std and windows headers
#include <iostream>
#include <fstream> 
#include <vector>

#define _USE_MATH_DEFINES // for C++
#include <cmath>
#include <cstdio>
#include <ctime>

// library headers
#include "xpicoscope.h"
#include "xtensor/xarray.hpp"
#include "xtensor/xnpy.hpp"
#include "fmt/core.h"
#include "kiss_fftr.h"
#include "tinyfiledialogs.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "nlohmann/json.hpp"

using json = nlohmann::json;

#include "imgui.h"
#include "implot.h"
#include "imApp.h"

std::map<std::string, XPS_CHANNEL> mapChannel = {
  { "A", XPS_channelA },
  { "B", XPS_channelB },
  { "C", XPS_channelC },
  { "D", XPS_channelD },
  { "EXT", XPS_EXT }
};

namespace ImGui {
  bool SliderDouble(const char* label, double* v, double v_min, double v_max, const char* format = NULL, ImGuiSliderFlags flags = 0) {
    return SliderScalar(label, ImGuiDataType_Double, v, &v_min, &v_max, format, flags);
  }
}

/// Generates hamming window of size L
inline xt::xarray<float> hamming(std::size_t L)
{
  xt::xarray<float> window = xt::empty<float>(xt::xarray<float>::shape_type({ L }));

  for (std::size_t w = 0; w < L; ++w)
    window[w] = (0.54f - 0.46f * (float)(std::cos(2 * M_PI * w / (L - 1))));

  return window;
}


// Generates hann window of size L
inline xt::xarray<float> hann(std::size_t L)
{
  xt::xarray<float> window = xt::empty<float>(xt::xarray<float>::shape_type({ L }));

  for (std::size_t w = 0; w < L; ++w)
    window[w] = (0.5f - 0.5f * (float)(std::cos(2 * M_PI * w / (L - 1))));

  return window;
}

class MainFrame : public imApp
{
public:
  MainFrame();
  ~MainFrame();

  void atStart();
  void Update();
  void atExit();

private:
  void showMainMenuBar();
  void readConfigFile();
  void saveConfigFile();
  bool checkError(bool retval, std::string funcstr);
  void showError(std::string errorString, bool exitAsWell=false);
  float* hanning(int N, short itype);
  void startMeasurement();

private:
  int16_t* mBufferA; // pointer to application buffer
  int16_t* mBufferB; // pointer to application buffer
  int16_t* mBufferC; // pointer to application buffer
  int16_t* mBufferD; // pointer to application buffer
  uint32_t mBufferLen; // length of application buffer

  xt::xarray<int16_t> mBufferA_all; // pointer to application buffer
  xt::xarray<int16_t> mBufferB_all; // pointer to application buffer
  xt::xarray<int16_t> mBufferC_all; // pointer to application buffer
  xt::xarray<int16_t> mBufferD_all; // pointer to application buffer
  uint64_t mBufferLen_all; // length of application buffer
  uint64_t mBufferPointer_all; 
  uint64_t mBufferMaxLength; 


  xt::xarray<float> mFFTA_all; // pointer to application buffer

  xt::xarray<float> mSpectrogramA;  // spectrogram

    // channel file descriptor
  std::ofstream chAFile;
  std::ofstream chBFile;
  std::ofstream chCFile;
  std::ofstream chDFile;
  std::ifstream chAFileIn;
  std::ifstream chBFileIn;
  std::ifstream chCFileIn;
  std::ifstream chDFileIn;

  int32_t mSamples;

  // start of recording
  double mStartTime;

  // true, if there is no picoscope present
  bool isDemo;

  // parameters for DAQ
  uint32_t DAQbits;
  uint32_t DAQFrequency_Hz;
  float DAQTime_s;
  bool DAQPeriodic;
  int32_t DAQRange_mV;
  int32_t DAQMaxADC;
  int16_t DAQNumberChannels;
  std::string DAQPath;

  // trigger parameters
  bool TriggerEnabled;
  XPS_CHANNEL TriggerChannel;
  int32_t TriggerThreshold;
  bool TriggerRising;

  // parameters spectrogram
  uint32_t binsFFT;
  uint32_t binsTime;
  uint32_t downsample;
  double specWindowMax;

  // plot parameters
  int32_t binsChanX;

  xt::xarray<float> xs;
  xt::xarray<float> xs5;
  xt::xarray<float> yA;
  xt::xarray<float> yB;
  xt::xarray<float> yC;
  xt::xarray<float> yD;
  xt::xarray<float> yF;

  // kissFFT variables 
  kiss_fftr_cfg kissFFTcfg;
  kiss_fft_cpx* kissFFTout;

  // hanning window function (here symmetric)
  xt::xarray<float> hanwin;

  int plotLen;

  double mMindB;                        // minimum spectrogram dB
  double mMaxdB;                        // maximum spectrogram dB

  // logging
  std::shared_ptr<spdlog::logger> logger;
};


MainFrame::MainFrame() :
  imApp("XARION-DAQ - (c) XARION Laser Acoustics GmbH", 1024, 720),
  mBufferA(NULL),
  mBufferB(NULL),
  mBufferC(NULL),
  mBufferD(NULL),
  mBufferLen(0),
  mBufferLen_all(0),
  mBufferPointer_all(0),
  binsFFT(0),
  binsTime(0),
  binsChanX(0),
  kissFFTout(NULL),
  kissFFTcfg(NULL),
  isDemo(false),
  mStartTime(0.0f),
  mMindB(50),
  mMaxdB(120),
  TriggerRising(true),
  mBufferMaxLength(0)
{
  // create log file
  try
  {
    logger = spdlog::basic_logger_mt("Insignia-DAQ Log", "log.txt");
  }
  catch (const spdlog::spdlog_ex& ex)
  {
    showError(std::string("Log init failed: ") + ex.what(), true);
  }
  spdlog::set_default_logger(logger);
  spdlog::set_level(spdlog::level::debug);
  spdlog::set_pattern("[%d.%m.%C %H:%M:%S.%e%z] [%l] %v");
  spdlog::info("Starting...");

  readConfigFile();

  mBufferLen_all = DAQFrequency_Hz;
  mBufferMaxLength = (uint64_t)(DAQFrequency_Hz * DAQTime_s);

  // create hanning window
  hanwin = hann(binsFFT * 2);

  kissFFTcfg = kiss_fftr_alloc(binsFFT*2, 0, NULL, NULL);
  kissFFTout = new kiss_fft_cpx[binsFFT + 1];
}


MainFrame::~MainFrame()
{
  if (chAFile.is_open()) chAFile.close();
  if (chBFile.is_open()) chBFile.close();
  if (chCFile.is_open()) chCFile.close();
  if (chDFile.is_open()) chDFile.close();

  if (chAFileIn.is_open()) chAFileIn.close();
  if (chBFileIn.is_open()) chBFileIn.close();
  if (chCFileIn.is_open()) chCFileIn.close();
  if (chDFileIn.is_open()) chDFileIn.close();

  if (kissFFTcfg)
    free(kissFFTcfg);

  if (kissFFTout)
    delete kissFFTout;

  spdlog::shutdown();
}


void MainFrame::showError(std::string errorString, bool exitAsWell)
{
  // log message
  spdlog::error(errorString);

  // show dialog
  tinyfd_messageBox("Error", errorString.c_str(), "ok", "error", 1);

  if (exitAsWell) 
  {
    spdlog::info("Exit after error");
    done=true;
  }
}


void MainFrame::readConfigFile()
{
  json jConfig;
  std::string logFilename = "config.json";

  // open config file
  spdlog::info("Read configuration file: {}", logFilename);
  std::ifstream ifs{ logFilename };
  if (ifs.fail())
  {
    showError("Unable to open config.json in same directory!", true);
    exit(EXIT_FAILURE);
  }

  // read/parse config file
  try
  {
    jConfig = json::parse(ifs);

    // get common settings
    isDemo = jConfig["demo"].get<bool>();
    if(isDemo)
      spdlog::info("Demo mode activated");

    // get DAQ settings
    DAQbits = jConfig["DAQ"]["bits"].get<uint32_t>();
    DAQFrequency_Hz = jConfig["DAQ"]["frequency_Hz"].get<uint32_t>();
    DAQRange_mV = jConfig["DAQ"]["range_mV"].get<int32_t>();
    DAQTime_s = jConfig["DAQ"]["time_s"].get<float>();
    DAQPeriodic = jConfig["DAQ"]["periodic"].get<bool>();
    DAQPath = jConfig["DAQ"]["savePath"].get<std::string>();
    DAQNumberChannels = jConfig["DAQ"]["noChannels"].get<int16_t>();

    if((DAQNumberChannels != 0) && (DAQNumberChannels != 2) && (DAQNumberChannels != 4))
      throw std::runtime_error(std::string("noChannels must be 0 (auto) or 2, 4"));

    spdlog::info("DAQ: f={}Hz, time={}s, {}", DAQFrequency_Hz, DAQTime_s, DAQPeriodic ? "periodic" : "non periodic");

    // get FFT settings
    binsFFT = jConfig["FFT"]["binsFFT"].get<uint32_t>();
    binsTime = jConfig["FFT"]["binsTime"].get<uint32_t>();
    spdlog::info("FFT bins={}, time bins={}", binsFFT, binsTime);

    // get plot settings
    binsChanX = jConfig["plot"]["binsChanX"].get<int32_t>();
    mMindB = jConfig["plot"]["mindB"].get<double>();
    mMaxdB = jConfig["plot"]["maxdB"].get<double>();
    downsample = jConfig["plot"]["downsample"].get<uint32_t>();
    specWindowMax = jConfig["plot"]["window_s"].get<double>();
    spdlog::info("plot bins={}", binsChanX);

    if (specWindowMax > DAQTime_s)
      specWindowMax = DAQTime_s;

    // trigger settings
    TriggerEnabled = jConfig["trigger"]["enabled"].get<bool>();
    std::string triggerChannel = jConfig["trigger"]["channel"].get<std::string>();
    if (mapChannel.find(triggerChannel) != mapChannel.end())
      TriggerChannel = mapChannel[triggerChannel];
    else
      throw new std::runtime_error("Unkown trigger channel!");
    TriggerThreshold = jConfig["trigger"]["threshold_mV"].get<int>();
    TriggerRising = jConfig["trigger"]["rising"].get<bool>();
  }
  catch (std::exception& e)
  {
    showError(fmt::format("Config JSON syntax error: {}!", e.what()), true);
    exit(EXIT_FAILURE);
  }
}


void MainFrame::saveConfigFile()
{
}


bool MainFrame::checkError(bool retval, std::string funcstr)
{
  if (retval == false)
  {
    char errorMessage[1024];
    getErrorMessage(errorMessage, sizeof(errorMessage) - 1);

    std::string message = "Error in function " + funcstr + "!" + '\n';
    message += "Closing device and exiting.";
    message += "\n\n";
    message += "Error: " + std::string(errorMessage) + '\n';

    close();

    showError(message, true);
  }

  return retval;
}


void MainFrame::atExit()
{
  bool retval = false;

  if (isOpen())
  {
    retval = stopMeasurement();
    checkError(retval, "stopMeasurement");
  }

  // close PicoScope Device
  retval = close();
  checkError(retval, "close");
}


void MainFrame::atStart()
{
  bool retval = false;

  // activate demo
  if (isDemo)
    activateDemo();

  // preparation PicoScope Device
  retval = setDeviceSettings(DAQbits, (int)(1e9/DAQFrequency_Hz));
  if(checkError(retval, "setDeviceSettings") == false) return;
  
  // open device
  retval = open();    // open PicoScope device
  if (checkError(retval, "open") == false) return;

  int16_t noChannels;
  retval = getNumberChannels(&noChannels);    // open PicoScope device
  if (checkError(retval, "getNumberChannels") == false) return;

  if ((DAQNumberChannels == 0) || (noChannels < DAQNumberChannels))
    DAQNumberChannels = noChannels;

  mBufferA_all = xt::zeros<int16_t>({ mBufferLen_all });
  mBufferB_all = xt::zeros<int16_t>({ mBufferLen_all });
  if (DAQNumberChannels > 2)
  {
    mBufferC_all = xt::zeros<int16_t>({ mBufferLen_all });
    mBufferD_all = xt::zeros<int16_t>({ mBufferLen_all });
  }

  mFFTA_all = xt::zeros<float>({ binsFFT + 1 });

  // resize spectrogram, zero values
  mSpectrogramA = xt::zeros<float>({ binsFFT * binsTime });

  yA = xt::zeros<float>({ DAQFrequency_Hz / 30 });
  yB = xt::zeros<float>({ DAQFrequency_Hz / 30 });
  if (DAQNumberChannels > 2)
  {
    yC = xt::zeros<float>({ DAQFrequency_Hz / 30 });
    yD = xt::zeros<float>({ DAQFrequency_Hz / 30 });
  }
  yF = xt::zeros<float>({ binsFFT*2 });

  xs = xt::linspace<float>(0, 1.0f / 30.0f, DAQFrequency_Hz / 30);

  xs5 = xt::linspace<float>(0.0f, DAQFrequency_Hz / 2.0f / 1000.0f, binsFFT);
    
  retval = setChannelSettings(1, true, true, DAQRange_mV, XPS_AC);             //Channel 1, Range 1000mV, available, enabled
  if (checkError(retval, "setChannelSettings A") == false) return;
  retval = setChannelSettings(2, true, true, DAQRange_mV, XPS_AC);            //Channel 2, Range 1000mV, available, not enabled
  if (checkError(retval, "setChannelSettings B") == false) return;

  retval = setChannelSettings(3, DAQNumberChannels > 2 ? true : false, DAQNumberChannels > 2 ? true : false, DAQRange_mV, XPS_AC);           //Channel 3, Range 1000mV, available, not enabled
  if (checkError(retval, "setChannelSettings C") == false) return;
  retval = setChannelSettings(4, DAQNumberChannels > 2 ? true : false, DAQNumberChannels > 2 ? true : false, DAQRange_mV, XPS_AC);           //Channel 4, Range 1000mV, available, not enabled
  if (checkError(retval, "setChannelSettings D") == false) return;

  retval = setMeasurementSettings(1e6*DAQTime_s, 1);
  if (checkError(retval, "setMeasurementSettings") == false) return;

  retval = setTriggerSettings(TriggerEnabled, TriggerThreshold, TriggerRising, 0.0, 0.0, TriggerChannel);
  if (checkError(retval, "setTriggerSettings") == false) return;
}


void MainFrame::startMeasurement()
{
  bool retval = false;

  retval = init(STREAM);
  if (checkError(retval, "init") == false) return;

  retval = getAppBuffer(1, &mBufferA, &mBufferLen);
  if (checkError(retval, "getAppBuffer A") == false) return;
  retval = getAppBuffer(2, &mBufferB, &mBufferLen);
  if (checkError(retval, "getAppBuffer B") == false) return;

  if (DAQNumberChannels > 2)
  {
    retval = getAppBuffer(3, &mBufferC, &mBufferLen);
    if (checkError(retval, "getAppBuffer C") == false) return;
    retval = getAppBuffer(4, &mBufferD, &mBufferLen);
    if (checkError(retval, "getAppBuffer D") == false) return;
  }

  retval = getMaxADC(&DAQMaxADC);
  if (checkError(retval, "getMaxADC") == false) return;

  retval = runMeasurement();
  if (checkError(retval, "runMeasurement") == false) return;

  // negative value, set to correct value if trigger occured
  mStartTime = -1.0;
}


void MainFrame::Update()
{
  int retval;

  {
    static uint32_t b = 0;
    static uint64_t plotChannelPointer = 0;
    static uint64_t oldPlotChannelPointer = 0;
    static double streamLastTime = -10.0;
    static const uint64_t buffer2Bin = (uint64_t)(DAQFrequency_Hz * DAQTime_s / binsTime);
    static uint64_t bufferPointer_FFT = 0;
    static float spectogramMaxA = -1.0f;
    static float spectogramMaxB = -1.0f;
    static float spectogramMaxC = -1.0f;
    static float spectogramMaxD = -1.0f;
    static double dragLinePosX = (DAQTime_s > specWindowMax ? specWindowMax : DAQTime_s )/2.0;
    static double dragLineChA = 1 / 60.0;
    static double dragLineChB = 1 / 60.0;
    static double dragLineChC = 1 / 60.0;
    static double dragLineChD = 1 / 60.0;
    static bool streamFinished = true;
    static bool restartFlag = false;
    static time_t startEpochTime;
    static double specXMin = 0.0;
    static double specXMax = DAQTime_s > specWindowMax ? specWindowMax : DAQTime_s;
    static double oldSpecXMin = 0.0;
    static double oldSpecSize = specXMax - specXMin;
    static bool correctAxis = true;
    static uint64_t overflow = 0;
    const uint32_t cDateTimeSize = 128;
    static char cDateTime[cDateTimeSize];

    float framerate = ImGui::GetIO().Framerate;
    
    // ask for data - if received, pause for 1/20s, since we only get every 1/15s something from picoScope
    if (((ImGui::GetTime() - streamLastTime) > 1.0 / 20.0) && ((!streamFinished) || restartFlag))
    {
      // update App Buffer
      retval = getLatestStreamingValues();
      checkError(retval, "getLatestStreamingValues");

      retval = getLastNumberOfSamples(&mSamples);
      checkError(retval, "getLastNumberOfSamples");

      // if there is date, pause
      if(mSamples!=0)
        streamLastTime = ImGui::GetTime();
    }
    else
      mSamples = 0;

    if (mStartTime < 0.0 && mSamples != 0)
    {
      mStartTime = ImGui::GetTime();
      startEpochTime = time(NULL);

      // create string timestamp from start timestamp
      tm curr_tm;
      localtime_s(&curr_tm, &startEpochTime);
      strftime(cDateTime, cDateTimeSize, "%Y%m%d_%H%M%S", &curr_tm);

      chAFile.open(fmt::format("{}\\{}_{}.bin", DAQPath, "channelA", cDateTime), std::ios::out | std::ios::binary);
      if (!chAFile.is_open()) showError(fmt::format("File '{}\\{}_{}.bin' couldn't be opened.", DAQPath, "channelA", cDateTime), true);
      chBFile.open(fmt::format("{}\\{}_{}.bin", DAQPath, "channelB", cDateTime), std::ios::out | std::ios::binary);
      if (!chBFile.is_open()) showError(fmt::format("File '{}\\{}_{}.bin' couldn't be opened.", DAQPath, "channelB", cDateTime), true);
      if (DAQNumberChannels > 2)
      {
        chCFile.open(fmt::format("{}\\{}_{}.bin", DAQPath, "channelC", cDateTime), std::ios::out | std::ios::binary);
        if (!chCFile.is_open()) showError(fmt::format("File '{}\\{}_{}.bin' couldn't be opened.", DAQPath, "channelC", cDateTime), true);
        chDFile.open(fmt::format("{}\\{}_{}.bin", DAQPath, "channelD", cDateTime), std::ios::out | std::ios::binary);
        if (!chDFile.is_open()) showError(fmt::format("File '{}\\{}_{}.bin' couldn't be opened.", DAQPath, "channelD", cDateTime), true);
      }

      mBufferMaxLength = (uint64_t)(DAQFrequency_Hz * DAQTime_s);

      streamFinished = false;
      restartFlag = false;

      bufferPointer_FFT = 0;
    }

    // copy data for later and conduct FFT
    if (mSamples > 0)
    {
      if (mSamples > mBufferLen_all-overflow)
      {
        xt::xarray<int16_t> tempArray = xt::zeros<int16_t>({ overflow });
        mBufferLen_all = mSamples + overflow;
        memcpy(tempArray.data(), mBufferA_all.data(), overflow * sizeof(mBufferA_all[0]));
        mBufferA_all.resize({ mBufferLen_all });
        memcpy(mBufferA_all.data(), tempArray.data(), overflow * sizeof(mBufferA_all[0]));

        memcpy(tempArray.data(), mBufferB_all.data(), overflow * sizeof(mBufferB_all[0]));
        mBufferB_all.resize({ mBufferLen_all });
        memcpy(mBufferB_all.data(), tempArray.data(), overflow * sizeof(mBufferB_all[0]));

        memcpy(tempArray.data(), mBufferC_all.data(), overflow * sizeof(mBufferC_all[0]));
        mBufferC_all.resize({ mBufferLen_all });
        memcpy(mBufferC_all.data(), tempArray.data(), overflow * sizeof(mBufferC_all[0]));

        memcpy(tempArray.data(), mBufferD_all.data(), overflow * sizeof(mBufferD_all[0]));
        mBufferD_all.resize({ mBufferLen_all });
        memcpy(mBufferD_all.data(), tempArray.data(), overflow * sizeof(mBufferD_all[0]));
      }

      memcpy(&mBufferA_all.data()[overflow], mBufferA, mSamples * sizeof(mBufferA[0]));
      memcpy(&mBufferB_all.data()[overflow], mBufferB, mSamples * sizeof(mBufferB[0]));
      chAFile.write((char*)(mBufferA), mSamples * sizeof(mBufferA[0]));
      chBFile.write((char*)(mBufferB), mSamples * sizeof(mBufferB[0]));
      if (DAQNumberChannels > 2)
      {
        memcpy(&mBufferC_all.data()[overflow], mBufferC, mSamples * sizeof(mBufferC[0]));
        memcpy(&mBufferD_all.data()[overflow], mBufferD, mSamples * sizeof(mBufferD[0]));
        chCFile.write((char*)(mBufferC), mSamples * sizeof(mBufferC[0]));
        chDFile.write((char*)(mBufferD), mSamples * sizeof(mBufferD[0]));
      }

      mBufferPointer_all += mSamples;
      mSamples += (uint32_t)overflow;

      for (uint32_t i = 0; i < DAQFrequency_Hz / 30; ++i) {
        yA[i] = (float)mBufferA_all[i] / DAQMaxADC * DAQRange_mV;
        yB[i] = (float)mBufferB_all[i] / DAQMaxADC * DAQRange_mV;
        if (DAQNumberChannels > 2)
        {
          yC[i] = (float)mBufferC_all[i] / DAQMaxADC * DAQRange_mV;
          yD[i] = (float)mBufferD_all[i] / DAQMaxADC * DAQRange_mV;
        }
      }
      
      if(mBufferMaxLength <= mBufferPointer_all)
      {
        if (isOpen())
        {
          retval = stopMeasurement();
          checkError(retval, "stopMeasurement");
        }

        if (chAFile.is_open()) chAFile.close();
        if (chBFile.is_open()) chBFile.close();
        if (chCFile.is_open()) chCFile.close();
        if (chDFile.is_open()) chDFile.close();

        mBufferMaxLength = mBufferPointer_all;

        streamFinished = true;

        chAFileIn.open(fmt::format("{}\\{}_{}.bin", DAQPath, "channelA", cDateTime), std::ios::in | std::ios::binary);
        if (!chAFileIn.is_open()) showError(fmt::format("File '{}\\{}_{}.bin' couldn't be opened.", DAQPath, "channelA", cDateTime), true);
        chBFileIn.open(fmt::format("{}\\{}_{}.bin", DAQPath, "channelB", cDateTime), std::ios::in | std::ios::binary);
        if (!chBFileIn.is_open()) showError(fmt::format("File '{}\\{}_{}.bin' couldn't be opened.", DAQPath, "channelB", cDateTime), true);
        if (DAQNumberChannels > 2)
        {
          chCFileIn.open(fmt::format("{}\\{}_{}.bin", DAQPath, "channelC", cDateTime), std::ios::in | std::ios::binary);
          if (!chCFileIn.is_open()) showError(fmt::format("File '{}\\{}_{}.bin' couldn't be opened.", DAQPath, "channelC", cDateTime), true);
          chDFileIn.open(fmt::format("{}\\{}_{}.bin", DAQPath, "channelD", cDateTime), std::ios::in | std::ios::binary);
          if (!chDFileIn.is_open()) showError(fmt::format("File '{}\\{}_{}.bin' couldn't be opened.", DAQPath, "channelD", cDateTime), true);
        }
      }

      uint64_t fftpointer = 0;
      while ((fftpointer + binsFFT * 2) < mSamples)
      {
        for (uint32_t i = 0; i < binsFFT * 2; ++i)
          yF[i] = mBufferA_all[fftpointer + i] * hanwin[i];
        kiss_fftr(kissFFTcfg, yF.data(), kissFFTout);
        for (uint32_t i = 0; i < binsFFT; ++i)
          mFFTA_all[i] = fabs(kissFFTout[i].i);

        fftpointer += binsFFT * 2;
        bufferPointer_FFT += binsFFT * 2;

        if (b < binsTime)
        {
          for (uint32_t i = 0; i < binsFFT; ++i)
          {
            mSpectrogramA[(binsFFT - 1 - i) * binsTime + b] = mFFTA_all[i] > mSpectrogramA[(binsFFT - 1 - i) * binsTime + b] ? mFFTA_all[i] : mSpectrogramA[(binsFFT - 1 - i) * binsTime + b];
          }
        }

        if (bufferPointer_FFT > (b + 1) * buffer2Bin)
        {
          for (uint32_t i = 0; i < binsFFT; ++i)
          {
            float temp;

            temp = mSpectrogramA[(binsFFT - 1 - i) * binsTime + b] = 20 * log10f(std::abs(mSpectrogramA[(binsFFT - 1 - i) * binsTime + b]));
            if (temp > spectogramMaxA) spectogramMaxA = temp;
          }
          b++;
        }
      }
      overflow = mSamples - fftpointer;
      for (uint32_t i = 0; i < overflow; i++)
      {
        mBufferA_all[i] = mBufferA_all[fftpointer + i];
        mBufferB_all[i] = mBufferB_all[fftpointer + i];
        if (DAQNumberChannels > 2)
        {
          mBufferC_all[i] = mBufferC_all[fftpointer + i];
          mBufferD_all[i] = mBufferD_all[fftpointer + i];
        }
      }
    }

    // prepare channel data for plotting
    if (((ImGui::GetTime() - mStartTime) > 1.0 / 15.0) && (mBufferPointer_all > DAQFrequency_Hz / 30))
    {
      if (streamFinished)
      {
        plotChannelPointer = (uint64_t)(((dragLinePosX-1.0/60.0)<0 ? 0.0 : (dragLinePosX - 1.0 / 60.0)) * DAQFrequency_Hz);

        if (plotChannelPointer != oldPlotChannelPointer)
        {
          if (mBufferLen_all != (uint64_t)(DAQFrequency_Hz) / 30)
          {
            mBufferLen_all = (uint64_t)(DAQFrequency_Hz) / 30;

            mBufferA_all.resize({ mBufferLen_all });
            mBufferB_all.resize({ mBufferLen_all });
            if (DAQNumberChannels > 2)
            {
              mBufferC_all.resize({ mBufferLen_all });
              mBufferD_all.resize({ mBufferLen_all });
            }
          }

          chAFileIn.seekg(plotChannelPointer * 2);
          chAFileIn.read(reinterpret_cast<char*>(mBufferA_all.data()), mBufferLen_all * 2);
          chBFileIn.seekg(plotChannelPointer * 2);
          chBFileIn.read(reinterpret_cast<char*>(mBufferB_all.data()), mBufferLen_all * 2);
          if (DAQNumberChannels > 2)
          {
            chCFileIn.seekg(plotChannelPointer * 2);
            chCFileIn.read(reinterpret_cast<char*>(mBufferC_all.data()), mBufferLen_all * 2);
            chDFileIn.seekg(plotChannelPointer * 2);
            chDFileIn.read(reinterpret_cast<char*>(mBufferD_all.data()), mBufferLen_all * 2);
          }

          for (uint32_t i = 0; i < binsFFT * 2; ++i)
            yF[i] = mBufferA_all[i] * hanwin[i];
          kiss_fftr(kissFFTcfg, yF.data(), kissFFTout);
          for (uint32_t i = 0; i < binsFFT; ++i)
            mFFTA_all[i] = fabs(kissFFTout[i].i);

          for (uint32_t i = 0; i < DAQFrequency_Hz / 30; ++i) {
            yA[i] = (float)mBufferA_all[i] / DAQMaxADC * DAQRange_mV;
            yB[i] = (float)mBufferB_all[i] / DAQMaxADC * DAQRange_mV;
            if (DAQNumberChannels > 2)
            {
              yC[i] = (float)mBufferC_all[i] / DAQMaxADC * DAQRange_mV;
              yD[i] = (float)mBufferD_all[i] / DAQMaxADC * DAQRange_mV;
            }
          }

          oldPlotChannelPointer = plotChannelPointer;
        }
      }
    }

    // ImGui Windows uses complete Frame, no menu bar, window decoration
    ImVec2 winSize = GetWindowSize();
    ImGui::SetNextWindowPos({ 0,0 }, ImGuiCond_Always);
    ImGui::SetNextWindowSize(winSize, ImGuiCond_Always);
    ImGui::Begin("Insignia-DAQ", NULL, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoTitleBar);

    // show time, framerate
    if (ImGui::Button(streamFinished ? "Start" : "-----") && streamFinished)
    {
      startMeasurement();
      mSamples = 0;
      streamLastTime = -10.0;
      restartFlag = true;
      specXMin = 0.0;
      specXMax = DAQTime_s > specWindowMax ? specWindowMax : DAQTime_s;
      oldSpecXMin = 0.0;
      oldSpecSize = specXMax - specXMin;
      correctAxis = true;
      spectogramMaxA = -1.0f;
      spectogramMaxB = -1.0f;
      spectogramMaxC = -1.0f;
      spectogramMaxD = -1.0f;
      dragLinePosX = (DAQTime_s > specWindowMax ? specWindowMax : DAQTime_s) / 2.0;
      dragLineChA = 1 / 60.0;
      dragLineChB = 1 / 60.0;
      dragLineChC = 1 / 60.0;
      dragLineChD = 1 / 60.0;
      mBufferLen_all = DAQFrequency_Hz;
      mBufferPointer_all = 0;
      overflow = 0;
      b = 0;
      mBufferA_all = xt::zeros<int16_t>({ mBufferLen_all });
      mBufferB_all = xt::zeros<int16_t>({ mBufferLen_all });
      mFFTA_all = xt::zeros<float>({ binsFFT });
      mSpectrogramA = xt::zeros<float>({ binsFFT * binsTime });
      if (DAQNumberChannels > 2)
      {
        mBufferC_all = xt::zeros<int16_t>({ mBufferLen_all });
        mBufferD_all = xt::zeros<int16_t>({ mBufferLen_all });
      }

      if (chAFileIn.is_open()) chAFileIn.close();
      if (chBFileIn.is_open()) chBFileIn.close();
      if (chCFileIn.is_open()) chCFileIn.close();
      if (chDFileIn.is_open()) chDFileIn.close();
    }
    ImGui::SameLine();
    if (!streamFinished)
    {
      if(mStartTime<0)
        ImGui::Text("Recording time: %.3f/%.3f", 0.0, DAQTime_s);
      else
        ImGui::Text("Recording time: %.3f/%.3f", ImGui::GetTime() - mStartTime < DAQTime_s ? ImGui::GetTime() - mStartTime : DAQTime_s, DAQTime_s);
    }
    else
    {
      ImGui::Text("Recording time: %.3f/%.3f", dragLinePosX, DAQTime_s);
    }
    ImGui::SameLine();
    if(mBufferPointer_all==0)
      ImGui::Text("| Press start...");
    else if (streamFinished == false && restartFlag == true)
      ImGui::Text("| Waiting for trigger...");
    else if(!streamFinished)
      ImGui::Text("| Recording...");
    else
      ImGui::Text("| Recording finished.");
    ImGui::SameLine();
    ImGui::Text("| Framerate: %.1f", framerate);
    ImGui::SameLine(ImGui::GetWindowWidth() - 122 - 210);
    ImGui::PushItemWidth(200); ImGui::SliderDouble("##SpecStart", &specXMin, 0, DAQTime_s);

    if (oldSpecXMin != specXMin)
    {
      correctAxis = true;
      oldSpecXMin = specXMin;
      specXMax = specXMin + oldSpecSize;
      oldSpecSize = specXMax - specXMin;
    }
    ImGui::SameLine(ImGui::GetWindowWidth() - 122);

    // button to change colormap
    static ImPlotColormap map = ImPlotColormap_Viridis;
    if (ImPlot::ColormapButton(ImPlot::GetColormapName(map), ImVec2(100, 0), map)) {
      map = (map + 1) % ImPlot::GetColormapCount();
      ImPlot::BustColorCache("##Spectrogram"); // bust colormap for plot Spectrogram so that colormap is resampled
    }
    ImPlot::PushColormap(map);

    const float w = ImGui::GetContentRegionAvail().x - 100 - ImGui::GetStyle().ItemSpacing.x;
    const float h = 320.0f;
    if (ImPlot::BeginPlot("##Spectrogram", ImVec2(w, h), ImPlotFlags_NoMouseText)) {
      ImPlot::SetupAxes(NULL, NULL, ImPlotAxisFlags_NoGridLines | ImPlotAxisFlags_Lock, ImPlotAxisFlags_NoGridLines | ImPlotAxisFlags_Lock);
      if (correctAxis)
      {
        ImPlot::SetupAxisLimits(ImAxis_X1, specXMin, specXMax, ImGuiCond_Always);
        correctAxis = false;
      }
      ImPlot::SetupAxisFormat(ImAxis_X1, "%gs");
      ImPlot::SetupAxisLimits(ImAxis_Y1, 0, DAQFrequency_Hz / 2.0 / 1000, ImGuiCond_Once);
      ImPlot::SetupAxisFormat(ImAxis_Y1, "%gkHz");
      ImPlot::PlotHeatmap("##Heat", mSpectrogramA.data(), binsFFT, binsTime, mMindB, mMaxdB, NULL, { 0,0 }, { DAQTime_s, DAQFrequency_Hz / 2.0 / 1000 });
      ImPlotRect pL = ImPlot::GetPlotLimits();
      if (streamFinished)
      {
        ImPlotPoint pt = ImPlot::GetPlotMousePos();

        if (dragLinePosX < pL.X.Min) dragLinePosX = pL.X.Min;
        if (dragLinePosX > pL.X.Max) dragLinePosX = pL.X.Max;

        if (dragLinePosX > (DAQTime_s - 1.0f / 60.0f)) dragLinePosX = DAQTime_s - 1.0f / 60.0f;
        if (dragLinePosX < 0) dragLinePosX = 0.0f;

        ImPlot::DragLineX(0, &dragLinePosX, { 1,1,1,1 }, 1, ImPlotDragToolFlags_NoInputs);
        if (ImPlot::IsPlotHovered() && ImGui::IsMouseDown(0) && ImGui::IsKeyDown(ImGuiKey_LeftCtrl))
          dragLinePosX = pt.x < 0 ? 0.0 : (pt.x > pL.X.Max ? pL.X.Max : pt.x);

        if (dragLinePosX > (DAQTime_s - 1.0f / 60.0f)) dragLinePosX = DAQTime_s - 1.0f / 60.0f;
        if (dragLinePosX < 0) dragLinePosX = 0.0f;
      }
      else
      {
        if (pL.X.Max < ((DAQTime_s / binsTime * b) < DAQTime_s ? (DAQTime_s / binsTime * b) : DAQTime_s))
        {
          specXMin = pL.X.Min;
          specXMax = (DAQTime_s / binsTime * b) < DAQTime_s ? (DAQTime_s / binsTime * b) : DAQTime_s;
          if ((specXMax - specXMin) > specWindowMax)
            specXMin = specXMax - specWindowMax;

          oldSpecXMin = specXMin;
          oldSpecSize = specXMax - specXMin;
          correctAxis = true;
        }
      }

      if (pL.Size().x > specWindowMax)
      {
        specXMin = pL.X.Min + (pL.Size().x - specWindowMax) / 2.0;
        specXMax = pL.X.Max - (pL.Size().x - specWindowMax) / 2.0;
        correctAxis = true;
      }
      ImPlot::EndPlot();
    }
    ImGui::SameLine();
    ImPlot::ColormapScale("##Scale", mMindB, mMaxdB, { 100, h }, IMPLOT_AUTO, "%gdB");
    if (ImGui::IsItemHovered() && ImGui::IsMouseClicked(ImGuiMouseButton_Right))
      ImGui::OpenPopup("Range");
    if (ImGui::BeginPopup("Range")) {
      ImGui::SliderDouble("Max", &mMaxdB, mMindB+1, 180);
      ImGui::SliderDouble("Min", &mMindB, -100, mMaxdB-1);
      ImGui::EndPopup();
    }

    ImPlot::PopColormap();

    if (ImPlot::BeginPlot("##FFT", ImVec2(ImGui::GetWindowContentRegionWidth() * 0.75f, 200)))
    {
      ImPlot::SetupAxes("f [kHz]", "arb. units [1]");
      ImPlot::SetupAxisLimits(ImAxis_X1, xs5[0], xs5[binsFFT - 1], ImPlotCond_Once);
      ImPlot::SetupAxisLimits(ImAxis_Y1, 0, 1e6, ImPlotCond_Once);
      ImPlot::PlotLine("FFT Channel A", xs5.data(), mFFTA_all.data(), binsFFT);
      ImPlot::EndPlot();
    }
    ImGui::SameLine();
    float image_width = ImGui::GetWindowContentRegionWidth() * 0.25f - 10.0f;
    float image_height = (float)my_image_height * image_width / my_image_width;
    if (image_height > 190.0f)
    {
      image_height = 190.0f;
      image_width = my_image_width * image_height / my_image_height;
    }
    ImGui::BeginChild("##LogoChild", ImVec2(ImGui::GetWindowContentRegionWidth() * 0.25f, 200.0f));
    ImGui::Dummy(ImVec2(image_width, (190.0f-image_height)/2.0f));
    ImGui::Image((ImTextureID)my_texture_srv_gpu_handle.ptr, ImVec2(image_width, image_height));
    ImGui::EndChild();

    ImGui::BeginChild("##ChildL", ImVec2(ImGui::GetWindowContentRegionWidth() * 0.5f, DAQNumberChannels > 2 ? 400 + ImGui::GetStyle().ItemSpacing.y : 200));
    if (ImPlot::BeginPlot("##Channel 1", ImVec2(-1, 200)))
		{
      ImPlot::SetupAxes("t [s]", "U [mV]");
      ImPlot::SetupAxisLimits(ImAxis_X1, xs[0], xs[DAQFrequency_Hz / 30 -1], ImPlotCond_Once);
      ImPlot::SetupAxisLimits(ImAxis_Y1, -DAQRange_mV, DAQRange_mV, ImPlotCond_Once);
      ImPlot::NextColormapColor();
      ImPlot::PlotLine("Channel A", xs.data(), yA.data(), DAQFrequency_Hz / 30 / downsample, 0, sizeof(yA.data()[0]) * downsample);
      if (streamFinished)
      {
        ImPlotRect pL = ImPlot::GetPlotLimits();
        ImPlotPoint pt = ImPlot::GetPlotMousePos();

        ImPlot::DragLineX(100, &dragLineChA, { 1,1,1,1 }, 1, ImPlotDragToolFlags_NoInputs);
        if (ImPlot::IsPlotHovered() && ImGui::IsMouseDown(0) && ImGui::IsKeyDown(ImGuiKey_LeftCtrl))
          dragLineChA = pt.x < 0 ? 0.0 : (pt.x > xs[DAQFrequency_Hz / 30 - 1] ? xs[DAQFrequency_Hz / 30 - 1] : pt.x);

        ImPlot::PlotText(fmt::format("{:.6f}", ((dragLinePosX - 1.0 / 60.0) < 0 ? 0.0 : (dragLinePosX - 1.0 / 60.0)) + dragLineChA).c_str(), pL.X.Min + pL.X.Size() * 0.88, pL.Y.Min + pL.Y.Size() * 0.85);
      }
		  ImPlot::EndPlot();
		}
    if (DAQNumberChannels > 2)
    {
      if (ImPlot::BeginPlot("##Channel 3", ImVec2(-1, 200)))
      {
        ImPlot::SetupAxes("t [s]", "U [mV]");
        ImPlot::SetupAxisLimits(ImAxis_X1, xs[0], xs[DAQFrequency_Hz / 30 - 1], ImPlotCond_Once);
        ImPlot::SetupAxisLimits(ImAxis_Y1, -DAQRange_mV, DAQRange_mV, ImPlotCond_Once);
        ImPlot::NextColormapColor(); ImPlot::NextColormapColor(); ImPlot::NextColormapColor();
        ImPlot::PlotLine("Channel C", xs.data(), yC.data(), DAQFrequency_Hz / 30 / downsample, 0, sizeof(yC.data()[0])*downsample);
        if (streamFinished)
        {
          ImPlotRect pL = ImPlot::GetPlotLimits();
          ImPlotPoint pt = ImPlot::GetPlotMousePos();

          ImPlot::DragLineX(100, &dragLineChC, { 1,1,1,1 }, 1, ImPlotDragToolFlags_NoInputs);
          if (ImPlot::IsPlotHovered() && ImGui::IsMouseDown(0) && ImGui::IsKeyDown(ImGuiKey_LeftCtrl))
            dragLineChC = pt.x < 0 ? 0.0 : (pt.x > xs[DAQFrequency_Hz / 30 - 1] ? xs[DAQFrequency_Hz / 30 - 1] : pt.x);

          ImPlot::PlotText(fmt::format("{:.6f}", ((dragLinePosX - 1.0 / 60.0) < 0 ? 0.0 : (dragLinePosX - 1.0 / 60.0)) + dragLineChC).c_str(), pL.X.Min + pL.X.Size() * 0.88, pL.Y.Min + pL.Y.Size() * 0.85);
        }
        ImPlot::EndPlot();
      }
    }
    ImGui::EndChild();
    ImGui::SameLine();
    ImGui::BeginChild("##ChildR", ImVec2(0, DAQNumberChannels > 2 ? 400 + ImGui::GetStyle().ItemSpacing.y : 200));

    if (ImPlot::BeginPlot("##Channel 2", ImVec2(-1, 200)))
    {
      ImPlot::SetupAxes("t [s]", "U [mV]");
      ImPlot::SetupAxisLimits(ImAxis_X1, xs[0], xs[DAQFrequency_Hz / 30 - 1], ImPlotCond_Once);
      ImPlot::SetupAxisLimits(ImAxis_Y1, -DAQRange_mV, DAQRange_mV, ImPlotCond_Once);
      ImPlot::NextColormapColor(); ImPlot::NextColormapColor();
      ImPlot::PlotLine("Channel B", xs.data(), yB.data(), DAQFrequency_Hz / 30 / downsample, 0, sizeof(yB.data()[0])* downsample);
      if (streamFinished)
      {
        ImPlotRect pL = ImPlot::GetPlotLimits();
        ImPlotPoint pt = ImPlot::GetPlotMousePos();

        ImPlot::DragLineX(100, &dragLineChB, { 1,1,1,1 }, 1, ImPlotDragToolFlags_NoInputs);
        if (ImPlot::IsPlotHovered() && ImGui::IsMouseDown(0) && ImGui::IsKeyDown(ImGuiKey_LeftCtrl))
          dragLineChB = pt.x < 0 ? 0.0 : (pt.x > xs[DAQFrequency_Hz / 30 - 1] ? xs[DAQFrequency_Hz / 30 - 1] : pt.x);

        ImPlot::PlotText(fmt::format("{:.6f}", ((dragLinePosX - 1.0 / 60.0) < 0 ? 0.0 : (dragLinePosX - 1.0 / 60.0)) + dragLineChB).c_str(), pL.X.Min + pL.X.Size() * 0.88, pL.Y.Min + pL.Y.Size() * 0.85);
      }
      ImPlot::EndPlot();
    }
    if (DAQNumberChannels > 2)
    {
      if (ImPlot::BeginPlot("##Channel 4", ImVec2(-1, 200)))
      {
        ImPlot::SetupAxes("t [s]", "U [mV]");
        ImPlot::SetupAxisLimits(ImAxis_X1, xs[0], xs[DAQFrequency_Hz / 30 - 1], ImPlotCond_Once);
        ImPlot::SetupAxisLimits(ImAxis_Y1, -DAQRange_mV, DAQRange_mV, ImPlotCond_Once);
        ImPlot::NextColormapColor(); ImPlot::NextColormapColor(); ImPlot::NextColormapColor(); ImPlot::NextColormapColor();
        ImPlot::PlotLine("Channel D", xs.data(), yD.data(), DAQFrequency_Hz / 30 / downsample, 0, sizeof(yD.data()[0])* downsample);
        if (streamFinished)
        {
          ImPlotRect pL = ImPlot::GetPlotLimits();
          ImPlotPoint pt = ImPlot::GetPlotMousePos();

          ImPlot::DragLineX(100, &dragLineChD, { 1,1,1,1 }, 1, ImPlotDragToolFlags_NoInputs);
          if (ImPlot::IsPlotHovered() && ImGui::IsMouseDown(0) && ImGui::IsKeyDown(ImGuiKey_LeftCtrl))
            dragLineChD = pt.x < 0 ? 0.0 : (pt.x > xs[DAQFrequency_Hz / 30 - 1] ? xs[DAQFrequency_Hz / 30 - 1] : pt.x);

          ImPlot::PlotText(fmt::format("{:.6f}", ((dragLinePosX - 1.0 / 60.0) < 0 ? 0.0 : (dragLinePosX - 1.0 / 60.0)) + dragLineChD).c_str(), pL.X.Min + pL.X.Size() * 0.88, pL.Y.Min + pL.Y.Size() * 0.85);
        }
        ImPlot::EndPlot();
      }
    }
    ImGui::EndChild();

    if (ImGui::BeginTable("table1", DAQNumberChannels + 1, ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg | ImGuiTableFlags_BordersH | ImGuiTableFlags_BordersV |
                                       ImGuiTableFlags_BordersOuter | ImGuiTableFlags_BordersInner, ImVec2(ImGui::GetWindowContentRegionWidth() * 0.5f, -1)))
    {
      ImGui::TableSetupColumn("");
      ImGui::TableSetupColumn("-Channel A [us]");
      ImGui::TableSetupColumn("-Channel B [us]");
      if (DAQNumberChannels > 2)
      {
        ImGui::TableSetupColumn("-Channel C [us]");
        ImGui::TableSetupColumn("-Channel D [us]");
      }
      ImGui::TableHeadersRow();

      // Channel A
      ImGui::TableNextRow();
      ImGui::TableSetColumnIndex(0);
      ImGui::TextUnformatted("Channel A");
      ImGui::TableSetColumnIndex(1);
      ImGui::TextUnformatted("");
      ImGui::TableSetColumnIndex(2);
      ImGui::TextUnformatted(fmt::format("{:.1f}", (dragLineChA - dragLineChB)*1e6).c_str());
      if (DAQNumberChannels > 2)
      {
        ImGui::TableSetColumnIndex(3);
        ImGui::TextUnformatted(fmt::format("{:.1f}", (dragLineChA - dragLineChC) * 1e6).c_str());
        ImGui::TableSetColumnIndex(4);
        ImGui::TextUnformatted(fmt::format("{:.1f}", (dragLineChA - dragLineChD) * 1e6).c_str());
      }

      // Channel B
      ImGui::TableNextRow();
      ImGui::TableSetColumnIndex(0);
      ImGui::TextUnformatted("Channel B");
      ImGui::TableSetColumnIndex(1);
      ImGui::TextUnformatted(fmt::format("{:.1f}", (dragLineChB - dragLineChA) * 1e6).c_str());
      ImGui::TableSetColumnIndex(2);
      ImGui::TextUnformatted("");
      if (DAQNumberChannels > 2)
      {
        ImGui::TableSetColumnIndex(3);
        ImGui::TextUnformatted(fmt::format("{:.1f}", (dragLineChB - dragLineChC) * 1e6).c_str());
        ImGui::TableSetColumnIndex(4);
        ImGui::TextUnformatted(fmt::format("{:.1f}", (dragLineChB - dragLineChD) * 1e6).c_str());
      }

      if (DAQNumberChannels > 2)
      {
        // Channel C
        ImGui::TableNextRow();
        ImGui::TableSetColumnIndex(0);
        ImGui::TextUnformatted("Channel C");
        ImGui::TableSetColumnIndex(1);
        ImGui::TextUnformatted(fmt::format("{:.1f}", (dragLineChC - dragLineChA) * 1e6).c_str());
        ImGui::TableSetColumnIndex(2);
        ImGui::TextUnformatted(fmt::format("{:.1f}", (dragLineChC - dragLineChB) * 1e6).c_str());
        ImGui::TableSetColumnIndex(3);
        ImGui::TextUnformatted("");
        ImGui::TableSetColumnIndex(4);
        ImGui::TextUnformatted(fmt::format("{:.1f}", (dragLineChC - dragLineChD) * 1e6).c_str());

        // Channel D
        ImGui::TableNextRow();
        ImGui::TableSetColumnIndex(0);
        ImGui::TextUnformatted("Channel D");
        ImGui::TableSetColumnIndex(1);
        ImGui::TextUnformatted(fmt::format("{:.1f}", (dragLineChD - dragLineChA) * 1e6).c_str());
        ImGui::TableSetColumnIndex(2);
        ImGui::TextUnformatted(fmt::format("{:.1f}", (dragLineChD - dragLineChB) * 1e6).c_str());
        ImGui::TableSetColumnIndex(3);
        ImGui::TextUnformatted(fmt::format("{:.1f}", (dragLineChD - dragLineChC) * 1e6).c_str());
        ImGui::TableSetColumnIndex(4);
        ImGui::TextUnformatted("");
      }

      ImGui::EndTable();
    }

    ImGui::End();
  }
}


void MainFrame::showMainMenuBar()
{
  // not used in the moment
  if (ImGui::BeginMainMenuBar())
  {
    if (ImGui::BeginMenu("File"))
    {
      if (ImGui::MenuItem("Quit", "ALT+F4"))
      {
        done = true;
      }
      ImGui::EndMenu();
    }

    ImGui::EndMainMenuBar();
  }
}


// taken from http://toto-share.com/2012/07/cc-hanning-matlab-code/
/*  function w = hanning(varargin)
%   HANNING   Hanning window.
%   HANNING(N) returns the N-point symmetric Hanning window in a column
%   vector.  Note that the first and last zero-weighted window samples
%   are not included.
%
%   HANNING(N,'symmetric') returns the same result as HANNING(N).
%
%   HANNING(N,'periodic') returns the N-point periodic Hanning window,
%   and includes the first zero-weighted window sample.
%
%   NOTE: Use the HANN function to get a Hanning window which has the
%          first and last zero-weighted samples.ep
    itype = 1 --> periodic
    itype = 0 --> symmetric
    default itype=0 (symmetric)

    Copyright 1988-2004 The MathWorks, Inc.
%   $Revision: 1.11.4.3 $  $Date: 2007/12/14 15:05:04 $
*/

float* MainFrame::hanning(int N, short itype)
{
  int half, i, idx, n;
  float* w;

  w = (float*)calloc(N, sizeof(float));
  memset(w, 0, N * sizeof(float));

  if (itype == 1)	//periodic function
    n = N - 1;
  else
    n = N;

  if (n % 2 == 0)
  {
    half = n / 2;
    for (i = 0; i < half; i++) //CALC_HANNING   Calculates Hanning window samples.
      w[i] = (float)(0.5 * (1.0 - cos(2 * M_PI * (i + 1) / (n + 1))));

    idx = half - 1;
    for (i = half; i < n; i++) {
      w[i] = w[idx];
      idx--;
    }
  }
  else
  {
    half = (n + 1) / 2;
    for (i = 0; i < half; i++) //CALC_HANNING   Calculates Hanning window samples.
      w[i] = (float)(0.5 * (1 - cos(2 * M_PI * (i + 1) / (n + 1))));

    idx = half - 2;
    for (i = half; i < n; i++) {
      w[i] = w[idx];
      idx--;
    }
  }

  if (itype == 1)	//periodic function
  {
    for (i = N - 1; i >= 1; i--)
      w[i] = w[i - 1];
    w[0] = 0.0;
  }
  return(w);
}


// Main code
//int main(int, char**)
INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
  PSTR lpCmdLine, INT nCmdShow)
{
  MainFrame mMainFrame;

  mMainFrame.Run();

  return 0;
}
