
// standard libraries
#include <string>
#include <map>

// imGui and directx libraries
#include "imgui.h"
#include "implot.h"
#include "imgui_impl_win32.h"
#include "imgui_impl_dx12.h"
#include <d3d12.h>


// helper functions
bool LoadTextureFromFile(const unsigned char* image_data, int image_width, int image_height, ID3D12Device* d3d_device, D3D12_CPU_DESCRIPTOR_HANDLE srv_cpu_handle, ID3D12Resource** out_tex_resource, int* out_width, int* out_height);

// Barebones Application Framework
class imApp 
{
public:
  // Constructors
  imApp();
  imApp(std::string title, int w, int h);
    
  // Destructor.
  virtual ~imApp();

  void imInit();
    
  // Called at top of run
  virtual void atStart()
    { }
  // Called at top of run
  virtual void atExit()
    { }

  // Update, called once per frame.
  virtual void Update() = 0;
    
  // Runs the app.
  void Run();
    
  // Get window size
  ImVec2 GetWindowSize();

  bool done;

public:
  // xarion logo
  int my_image_width = 0;
  int my_image_height = 0;
  D3D12_CPU_DESCRIPTOR_HANDLE my_texture_srv_cpu_handle;
  D3D12_GPU_DESCRIPTOR_HANDLE my_texture_srv_gpu_handle;

private:
  void StyeColorsApp();

  HWND hwnd;
  WNDCLASSEX wc;

  int mWidth;
  int mHeight;

  std::string mTitle;
  ImVec4 ClearColor;                    // background clear color
  //std::map<std::string, ImFont*> Fonts;  // font map
};