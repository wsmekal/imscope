
#ifndef XPICOSCOPE_HEADER_H
#define XPICOSCOPE_HEADER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "xpicoscope_Export.h"

// std headers
#include <inttypes.h>
#include <stdbool.h> 

///////////////////////////////////////////////////////////////////////////////
// definitions of constants
///////////////////////////////////////////////////////////////////////////////

typedef enum erecordMode {
  STREAM=0,
  BLOCK,
  RAPIDBLOCK
} recordMode;

typedef enum enXPSCoupling
{
  XPS_AC = 0,
  XPS_DC
} XPS_COUPLING;

typedef enum enXPSchannel {
  XPS_EXT = 0,
  XPS_channelA = 1,
  XPS_channelB = 2,
  XPS_channelC = 3,
  XPS_channelD = 4
} XPS_CHANNEL;


///////////////////////////////////////////////////////////////////////////////
// declaration of functions privided by dll
///////////////////////////////////////////////////////////////////////////////

/*! Copies last error message to given buffer.
 *
 *  @author     Werner Smekal
 *  @date       2021/01/30
 *  @param[out] message char buffer, where error message is copied to.
 *  @param[in]  len length of char buffer
 *  return      true if successful
 */
xpicoscope_EXPORT bool getErrorMessage(char* message, size_t len);


/*! Copies last error message to given buffer.
 *
 *  @author     Werner Smekal
 *  @date       2021/01/30
 *  @param[out] message char buffer, where error message is copied to.
 *  @param[in]  len length of char buffer
 *  return      true if successful
 */
xpicoscope_EXPORT bool getNumberChannels(int16_t* number);


/*! Copies last error message to given buffer.
 *
 *  @author     Werner Smekal
 *  @date       2021/01/30
 *  @param[out] message char buffer, where error message is copied to.
 *  @param[in]  len length of char buffer
 *  return      true if successful
 */
xpicoscope_EXPORT bool getMaxADC(int32_t* maxADC);


/*! Get the number of samples read during the last call to getLatestStreamingValues().
 *
 *  @author     Werner Smekal
 *  @date       2021/03/09
 *  @param[out] nSamples pointer to int32_t variable
 *  return      true if successful
 */
xpicoscope_EXPORT bool getLastNumberOfSamples(int32_t* nSamples);


/*! Returns address of int16_t buffer which contains data of channel for streaming mode.
 *
 *  @author     Werner Smekal
 *  @date       2021/03/09
 *  @param[in]  channel number 1 to 4 corresponding to channel A to D
 *  @param[out] appBuffer pointer to int16_t* which contains pointer to buffer after call
 *  return      true if successful
 */
xpicoscope_EXPORT bool getAppBuffer(int channel, int16_t** appBuffer, uint32_t* bufferlen);


/*! Get the status of the latest function call.
 *
 *  @author     Werner Smekal
 *  @date       2021/03/09
 *  @param[out] status pointer to variable
 *  return      true if successful
 */
xpicoscope_EXPORT bool getStatus(uint32_t* status);


/*! For block mode or rapid block mode this function returns, if
 *  the device finished recording.
 *
 *  @author     Werner Smekal
 *  @date       2021/03/14
 *  @param[out] ready pointer to variable, variable will contain 0 if still collecting
 *  return      true if successful
 */
xpicoscope_EXPORT bool isDeviceReady(uint16_t* ready);


/*! Call this function regularily in streaming mode to trigger the copy of data from
 *  the device buffer to the app buffers.
 *
 *  @author     Werner Smekal
 *  @date       2021/03/09
 *  return      true if successful
 */
xpicoscope_EXPORT bool getLatestStreamingValues();


/*! Set vertical (bits) and horizontal (ns) resolution. 
 *
 *  @author     Werner Smekal
 *  @date       2021.03.01
 *  @param[in]  resolution_bits wanted resolution in bits (8, 12, 14, 15, 16 bits are valid)
 *  @param[in]  timeInterval_ns wanted time interval in ns
 *  return      true if successful
 */
xpicoscope_EXPORT bool setDeviceSettings(int resolution_bits, int timeInterval_ns);


/*! Set settings regarding measurment.
 *
 *  @author     Werner Smekal
 *  @date       2021.03.13
 *  @param[in]  scantime scan time in us for streaming mode or in (rapid) block mode for 1 (!) segment/block
 *  @param[in]  seg number of memory segments, equals max number of triggered measurements for rapid block mode
 *  return      true if successful
 */
xpicoscope_EXPORT bool setMeasurementSettings(double scantime, uint32_t seg);


/*! Change trigger settings.
 *
 *  @author     Werner Smekal
 *  @date       2021.03.09
 *  @param[in]  enabled True if trigger should be enabled
 *  @param[in]  threshold_mV threshold which needs to be surpassed for trigger to occur
 *  @param[in]  rising set to true for rising edge trigger, false for falling edge trigger
 *  @param[in]  delay_us delay in us after which recording starts
 *  @param[in]  preTrigger_us pretrigger recording time in us. delay will then be set to 0.
 *  @param[in]  channel which channel is used for trigger
 *  return      true if successful
 */
xpicoscope_EXPORT bool setTriggerSettings(bool enabled, int threshold_mV, bool rising, double delay_us, double preTrigger_us, XPS_CHANNEL channel);


/*! Change settings for channels A to D.
 *
 *  @author     Werner Smekal
 *  @date       2021.03.09
 *  @param[in]  channel number from 1 to 4 corresponding to channel A to D
 *  @param[in]  available true if channel is available (channel C and D might not be available for the PicoScope device
 *  @param[in]  enabled true if channel should record
 *  @param[in]  range_mV vertical range in mV (from 50 to 20000mV)
 *  return      true if successful
 */
xpicoscope_EXPORT bool setChannelSettings(int channel, bool available, bool enabled, long range_mV, XPS_COUPLING ac_dc);


/*! Is device open (handle of device driver not zero)?
 *
 *  @author     Werner Smekal
 *  @date       2021.07.09
 *  return      true if device driver is open/in use
 */
xpicoscope_EXPORT bool isOpen();


/*! Open device (get handle from device driver).
 *
 *  @author     Werner Smekal
 *  @date       2021.03.09
 *  return      true if successful
 */
xpicoscope_EXPORT bool open();


/*! Close PicoScope device and free all allocated memory.
 *
 *  @author     Werner Smekal
 *  @date       2021.03.09
 *  return      true if successful
 */
xpicoscope_EXPORT bool close();


/*! Initialize PicoScope device. Trigger, Device, Channel settings must have been done before.
 *
 *  @author     Werner Smekal
 *  @date       2021.03.09
 *  @param[in]  mode recording mode: STREAM, RAPIDBLOCK, BLOCK
 *  return      true if successful
 */
xpicoscope_EXPORT bool init(recordMode mode);


/*! Starts measurement.
 *
 *  @author     Werner Smekal
 *  @date       2021.03.09
 *  return      true if successful
 */
xpicoscope_EXPORT bool runMeasurement();


/*! Stops measurement if running. Call the function to make
 *  sure, PicoScope has stopped recording. For Block and RapidBlock 
 *  mode the buffer is copied to the Application Buffer.
 *
 *  @author     Werner Smekal
 *  @date       2021.03.09
 *  return      true if successful
 */
xpicoscope_EXPORT bool stopMeasurement();


/*! If set to true, one case use the library without picoscope device.
 *  This should be the first command to the library otherwise it will
 *  be ignored.
 *
 *  @author     Werner Smekal
 *  @date       2022.03.26
 *  return      true if successful
 */
xpicoscope_EXPORT bool activateDemo();

#ifdef __cplusplus
}
#endif

#endif /* XPICOSCOPE_HEADER_H */