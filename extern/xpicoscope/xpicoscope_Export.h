
#ifndef XPICOSCOPE_EXPORT_HEADER_H
#define XPICOSCOPE_EXPORT_HEADER_H

#ifdef XPICOSCOPE_STATIC_DEFINE
#  define xpicoscope_EXPORT
#  define XPICOSCOPE_NO_EXPORT
#else
#  ifndef xpicoscope_EXPORT
#    ifdef XPicoScopeDLL_EXPORTS
        /* We are building this library */
#      define xpicoscope_EXPORT __declspec(dllexport)
#    else
        /* We are using this library */
#      define xpicoscope_EXPORT __declspec(dllimport)
#    endif
#  endif

#  ifndef XPICOSCOPE_NO_EXPORT
#    define XPICOSCOPE_NO_EXPORT 
#  endif
#endif

#ifndef XPICOSCOPE_DEPRECATED
#  define XPICOSCOPE_DEPRECATED __declspec(deprecated)
#endif

#ifndef XPICOSCOPE_DEPRECATED_EXPORT
#  define XPICOSCOPE_DEPRECATED_EXPORT xpicoscope_EXPORT XPICOSCOPE_DEPRECATED
#endif

#ifndef XPICOSCOPE_DEPRECATED_NO_EXPORT
#  define XPICOSCOPE_DEPRECATED_NO_EXPORT XPICOSCOPE_NO_EXPORT XPICOSCOPE_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef XPICOSCOPE_NO_DEPRECATED
#    define XPICOSCOPE_NO_DEPRECATED
#  endif
#endif

#endif /* XPICOSCOPE_EXPORT_HEADER_H */
